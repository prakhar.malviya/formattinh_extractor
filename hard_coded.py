html_begin = '''<!DOCTYPE html>
<html lang="en">
<head>
<style>
.page {
    border-bottom: dashed;
    margin-top: 1%;
}

.UD {
    background-color: white;
    background-image: linear-gradient(90deg, rgba(200, 0, 0, .2) 5%, transparent 10%);
    background-size: 10px 10px;
}

.pageNo {
    background-color: gray;
}

.block {
    border: gold solid;
    min-width: 75%;
    width: fit-content;
    margin-left: 10%;
    padding: 1%;
}

.par {
    border: lightgreen ridge;
    display: block;
}

.line {
    display: inline-block;
    /*height: fit-content;*/
    width: 100%;
    padding: 0;
    border: groove;
}

.word {
    display: inline-block;
    /*border: thin lightcoral solid;*/
    margin: .2%;
    margin-top: .5%;
}

.invalid,
.joint {
    background-color: red;
}

.center {
    display: block;
    white-space: nowrap;
    margin-left: auto;
    margin-right: auto;
    /*margin-top: .1%;
    margin-bottom: .1%;*/
    width: fit-content;
}


/*.headerOrFooter{
    background-color: lightgreen;
}*/

.header {
    background-color: aqua;
}

.footer {
    background-color: lightslategray;
}

.docNo {
    font-size: large;
    font-weight: bold;
}

.document {
    border: solid thick;
    margin-top: 2%;
}

.section {
    border: solid yellowgreen;
    margin: 2%;
}

.encounter {
    border: solid teal thick;
    margin: 2%;
}

table {
    margin: auto;
    height: auto;
}

td {
    border: solid thin;
    width: 25%;
}


/*#sideBar>table>tr {
    border: solid thin;
    width: 25%;
}*/

#mySidebar>table>tbody>tr>td {
    border: none;
    width: auto;
    padding-top: 15px;
}


/*tr {
    margin: 2%;
}*/

.report {
    background-color: lightpink;
}


/*.recognized{
    background-color: greenyellow;
}*/

.field {
    background-color: black;
    font-weight: bolder;
    color: white;
    border: blue solid 2px;
    padding: 2px;
}

.value {
    background-color: white;
    color: black;
}

.icd {
    background-color: blueviolet;
    font-weight: bold;
}

.sticky {
    position: -webkit-sticky;
    /* Safari */
    position: sticky;
    top: 0;
    background-color: white;
}

.collapsible {
    /*background-color: #777;
    color: white;*/
    cursor: pointer;
    width: 100%;
    border: none;
    text-align: center;
    outline: none;
    /*font-size: 15px;*/
}

.active,
.collapsible:hover {
    background-color: #555;
}

.content {
    display: none;
    overflow: hidden;
}

.collapsible:after {
    content: '\02795';
    /* Unicode character for "plus" sign (+) */
    color: white;
    margin-left: 5px;
}

.active:after {
    content: "\2796";
    /* Unicode character for "minus" sign (-) */
}

.sidebar {
    height: 100%;
    /* 100% Full-height */
    width: 0;
    /* 0 width - change this with JavaScript */
    position: fixed;
    /* Stay in place */
    z-index: 1;
    /* Stay on top */
    top: 0;
    left: 0;
    background-color: whitesmoke;
    /* Black*/
    overflow-x: hidden;
    /* Disable horizontal scroll */
    padding-top: 60px;
    /* Place content 60px from the top */
    transition: 0.5s;
    /* 0.5 second transition effect to slide in the sidebar */
}


/* The sidebar links */

.sidebar a {
    padding: 8px 8px 8px 32px;
    text-decoration: none;
    font-size: 25px;
    color: black;
    display: block;
    transition: 0.3s;
}


/* When you mouse over the navigation links, change their color */

.sidebar a:hover {
    color: #f1f1f1;
}


/* Position and style the close button (top right corner) */

.sidebar .closebtn {
    position: absolute;
    top: 0;
    right: 25px;
    font-size: 36px;
    margin-left: 50px;
}


/* The button used to open the sidebar */

.openbtn {
    font-size: 20px;
    cursor: pointer;
    background-color: peru;
    color: white;
    padding: 10px 15px;
    border: none;
    box-shadow: 10px 10px 5px grey;
}

.openbtn:hover {
    background-color: #444;
}


/* Style page content - use this if you want to push the page content to the right when you open the side navigation */

#main {
    transition: margin-left .5s;
    /* If you want a transition effect */
    padding: 20px;
}


/* On smaller screens, where height is less than 450px, change the style of the sidenav (less padding and a smaller font size) */

@media screen and (max-height: 450px) {
    .sidebar {
        padding-top: 15px;
    }
    .sidebar a {
        font-size: 18px;
    }
}

input {
    margin: 8% 2% 8% 2%;
}

.corrected {
    background-color: green;
}
</style><meta charset="UTF-8">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<title>Nothing to see here</title>
<script>
$(document).ready(function() {
    var coll = document.getElementsByClassName("collapsible");
    var i;

    for (i = 0; i < coll.length; i++) {
        coll[i].addEventListener("click", function() {
            this.classList.toggle("active");
            var content = this.nextElementSibling;
            if (content.style.display === "block") {
                content.style.display = "none";
            } else {
                content.style.display = "block";
            }
        });
    }


    $('.toggle').change(function() {
        var val = $(this).attr('id');
        if (this.checked) {
            $("." + val + "0").addClass(val);
            $("." + val + "0").removeClass(val + "0");
        } else {
            $("." + val).addClass(val + "0");
            $("." + val).removeClass(val);

        }
    });
});


function openNav() {
    document.getElementById("mySidebar").style.width = "250px";
    //document.getElementById("main").style.marginLeft = "250px";
}

/* Set the width of the sidebar to 0 and the left margin of the page content to 0 */
function closeNav() {
    document.getElementById("mySidebar").style.width = "0";
    //document.getElementById("main").style.marginLeft = "0";
}
</script>
</head>
<body><button class="openbtn" onclick="openNav()" style="left:0px; top:0px; position:fixed;">&#9776; Menu</button>
    <div id="mySidebar" class="sidebar">
        <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a> <br>
        <table>
            <tr>
                <td>
                    <input type="checkbox" id="document" class="toggle" checked>
                </td>
                <td>
                    <span class="document" style="display:inline-block;">Document</span>
                </td>
            </tr>
            <tr>
                <td>
                    <input type="checkbox" id="encounter" class="toggle" checked>
                </td>
                <td>
                    <span class="encounter" style="display:inline-block;">Encounter</span>
                </td>
            </tr>
            <tr>
                <td>
                    <input type="checkbox" id="section" class="toggle" checked>
                </td>
                <td>
                    <span class="section" style="display:inline-block;">Section</span>
                </td>
            </tr>
            <tr>
                <td>
                    <input type="checkbox" id="page" class="toggle" checked>
                </td>
                <td>
                    <span class="page" style="display:inline-block;">Page</span>
                </td>
            </tr>
            <tr>
                <td>
                    <input type="checkbox" id="block" class="toggle" checked>
                </td>
                <td>
                    <span class="block" style="display:inline-block;">Block</span>
                </td>
            </tr>
            <tr>
                <td>
                    <input type="checkbox" id="par" class="toggle" checked>
                </td>
                <td>
                    <span class="par" style="padding: 5%; max-width: fit-content; display:inline;">Paragraph</span>
                </td>
            </tr>
            <tr>
                <td>
                    <input type="checkbox" id="line" class="toggle" checked>
                </td>
                <td>
                    <span class="line" style="padding: 5%; max-width: fit-content;">Sentence</span>
                </td>
            </tr>
            <tr>
                <td>
                    <input type="checkbox" id="field" class="toggle" checked>
                </td>
                <td>
                    <span class="field" style="display:inline-block;">Field
                        <span class="value">Value</span>
                    </span>
                </td>
            </tr>
            <tr>
                <td>
                    <input type="checkbox" id="icd" class="toggle" checked>
                </td>
                <td>
                    <span class="icd" style="display:inline-block;">ICD</span>
                </td>
            </tr>
            <tr>
                <td>
                    <input type="checkbox" id="invalid" class="toggle" checked>
                </td>
                <td>
                    <span class="invalid" style="display:inline-block;">Low word confidence</span>
                </td>
            </tr>
            <tr>
                <td>
                    <input type="checkbox" id="corrected" class="toggle" checked>
                </td>
                <td>
                    <span class="corrected" style="display:inline-block;">Dictionary corrected</span>
                </td>
            </tr>

        </table></div>'''

html_end = '</body></html>'
