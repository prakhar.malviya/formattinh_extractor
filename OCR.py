import os

from PIL import Image
from cv2 import cv2
from pytesseract import pytesseract

from Models.Block import Block
from Models.Page import Page
from Models.Paragraph import Paragraph
from Models.Sentence import Sentence
from Models.Word import Word


def process(id, imgfile, page_no, html_begin='', html_end='', preprocess=""):
    out_dir = "output"
    if not os.path.exists(out_dir):
        os.makedirs(out_dir, exist_ok=True)

    print("Running OCR on %s" % imgfile)

    image = cv2.imread(imgfile)
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    if preprocess == "thresh":
        gray = cv2.threshold(gray, 0, 255,
                             cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]

    elif preprocess == "blur":
        gray = cv2.medianBlur(gray, 3)

    filename = "processed_image_{}.png".format(id)
    cv2.imwrite(filename, gray)
    data = pytesseract.image_to_data(Image.open(filename))
    os.remove(filename)

    lines = data.splitlines()
    headers = lines.pop(0).split("\t")

    col_page = headers.index("page_num")
    col_block = headers.index("block_num")
    col_paragraph = headers.index("par_num")
    col_line = headers.index("line_num")
    col_left = headers.index("left")
    col_top = headers.index("top")
    col_width = headers.index("width")
    col_height = headers.index("height")
    col_text = headers.index("text")

    # print("Columns {}, {}, {}, {}, {}, {}, {}, {}".format(col_page, col_block, col_paragraph, col_left, col_top,
    #                                                      col_width,
    #                                                      col_height, col_text))

    pages = []

    page = Page(page_no)
    block = None
    paragraph = None
    sentence = None
    word_id = 0

    for line in lines:
        values = line.strip().split("\t")
        if len(values) == 12:

            page_id = int(values[col_page])
            block_id = int(values[col_block])
            paragraph_id = int(values[col_paragraph])
            line_id = int(values[col_line])

            if block is None or block.id != block_id:
                block = Block(block_id)
                page.add_block(block)
                paragraph = Paragraph(paragraph_id)
                block.add_pagagraph(paragraph)
                sentence = Sentence(line_id)
                paragraph.add_sentence(sentence)
                word_id = 0

            elif paragraph.id != paragraph_id:
                paragraph = Paragraph(paragraph_id)
                block.add_pagagraph(paragraph)
                sentence = Sentence(line_id)
                paragraph.add_sentence(sentence)
                word_id = 0

            elif sentence.id != line_id:
                sentence = Sentence(line_id)
                paragraph.add_sentence(sentence)
                word_id = 0

            left = int(values[col_left])
            top = int(values[col_top])

            right = left + int(values[col_width])
            bottom = top + int(values[col_height])

            text = values[col_text]

            word = Word(word_id, left, top, right, bottom, text)
            word_id = word_id + 1
            sentence.add_word(word)

    text_file = out_dir + '/' + imgfile.split('/')[-1] + '.html'
    f = open(text_file, "w+")
    f.write(html_begin + page.to_html() + html_end)
    f.close()
    return text_file
