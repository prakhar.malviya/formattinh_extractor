from Models.Word import Word


class Sentence:
    #    id = 0
    #    words = []

    def __init__(self, id):
        self.id = id
        self.words = []

    def add_word(self, word):
        assert type(word) == Word
        self.words.append(word)

    def __str__(self):
        txt = ""
        for word in self.words:
            txt = txt + word.text + " "
        return txt.strip()

    def to_html(self):
        txt = '<span class="line">'
        for word in self.words:
            txt = txt + word.to_html() + " "
        return txt.strip() + '</span>'
