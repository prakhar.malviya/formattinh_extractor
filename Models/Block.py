from Models.Paragraph import Paragraph


class Block:
    #    id = 0;
    #    paragraphs = []

    def __init__(self, id):
        self.id = id
        self.paragraphs = []

    def add_pagagraph(self, pagagraph):
        assert type(pagagraph) == Paragraph
        self.paragraphs.append(pagagraph)

    def __str__(self):
        txt = ""
        for paragraph in self.paragraphs:
            txt = txt + str(paragraph) + "\n"
        return txt.strip()

    def to_html(self):
        txt = '<div class="block">'
        for paragraph in self.paragraphs:
            txt = txt + paragraph.to_html() + "<br>\n"
        return txt.strip() + "</div>"
