class Word:
    """
    id = 0
    left = 0
    top = 0
    right = 0
    bottom = 0
    text = ""
    """

    def __init__(self, id, left, top, right, bottom, text):
        self.id = id
        self.left = left
        self.top = top
        self.right = right
        self.bottom = bottom
        self.text = text

    def to_html(self):
        return f'<span class="word">{self.text}</span>'


"""
    def __str__(self):
        return "Word: {}\t\t\tPage Number: {}\tBlock: {}\tParagraph:{}\tBounding box: ({}_{}_{}_{})".format(self.text,
                                                                                                            self.page,
                                                                                                            self.block,
                                                                                                            self.paragraph,
                                                                                                            self.left,
                                                                                                            self.top,
                                                                                                            self.right,
                                                                                                            self.bottom)
"""
