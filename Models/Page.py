from Models.Block import Block


class Page:
    #    id = 0
    #    blocks = []

    def __init__(self, id):
        self.id = id
        self.blocks = []

    def add_block(self, block):
        assert type(block) == Block
        self.blocks.append(block)

    def __str__(self):
        txt = "--------------------------------------------- Page {} -----------------------------------------\n".format(
            self.id)
        for block in self.blocks:
            txt = txt + str(block) + "\n"
        txt = txt + "--------------------------------------------- Page end -----------------------------------------"
        return txt

    def to_html(self):
        txt = '<div class="page">'
        txt = txt + "<div class='pageNo' style='text-align: center;'>Page number " + str(self.id) + "</div>"
        for block in self.blocks:
            txt = txt + block.to_html() + "<br>\n"
        txt = txt + '</div>'
        return txt
