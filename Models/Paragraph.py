from Models.Sentence import Sentence


class Paragraph:
    #    id = 0
    #    sentences = []

    def __init__(self, id):
        self.id = id
        self.sentences = []

    def add_sentence(self, sentence):
        assert type(sentence) == Sentence
        self.sentences.append(sentence)

    def __str__(self):
        txt = ""
        for sentence in self.sentences:
            txt = txt + str(sentence) + "\n"
        return txt.strip()

    def to_html(self):
        txt = '<div class="par">'
        for sentence in self.sentences:
            txt = txt + sentence.to_html() + '<br>\n'
        return txt.strip() + '</div>'
